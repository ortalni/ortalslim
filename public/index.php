<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

require __DIR__ . '/../vendor/autoload.php';

session_start();

// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);

// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Register middleware
require __DIR__ . '/../src/middleware.php';

// Register routes
require __DIR__ . '/../src/routes.php';


// Run app

//ex1

$app->get('/customers/{id}', function (Request $request, Response $response) {
	$json = '{"1":"john", "2":"jack"}';
	$array = (json_decode($json));
	$id = $request->getAttribute('id');
	if (isset($array->{$id})) {
		$result = $array->{$id};
	} else {
		$result = "User with id ". $id. " not exist.";	
	}
	
   $response->getBody()->write($result);
    return $response;
});

$app->run();

